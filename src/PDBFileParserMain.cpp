//============================================================================
// Name        : PDBFileParser.cpp
// Author      : JP
// Version     :
// Description : Test Rig for PDB File parser
//============================================================================

#include <iostream>

#include "PDBFileParser.h"

using namespace std;
using namespace RDKit;

int main() {
	cout << "Testing PDB File Parser" << endl;
	PDBFileParser pdbFileParser("../test/resources/1EZQ.pdb");
	return 0;
}
