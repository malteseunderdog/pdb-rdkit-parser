// $Id$
//
//  Copyright (C) 2012 JP
//
//  @@ All Rights Reserved @@
//  This file is part of the RDKit.
//  The contents are covered by the terms of the BSD license
//  which is included in the file license.txt, found at the root
//  of the RDKit source tree.
//

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/algorithm/string.hpp>

#include <RDGeneral/BadFileException.h>
#include <GraphMol/RDKitBase.h>
#include <GraphMol/SmilesParse/SmilesParse.h>
#include <GraphMol/SmilesParse/SmilesWrite.h>

#include "PDBFileParser.h"

#define BOND_THRESHOLD 1.9

using namespace std;

/*
    IMPORTANT ASSUMPTIONS (READ CAREFULLY)
    
    - PDB definition at http://www.wwpdb.org/documentation/format33/v3.3.html
    - ATOM records are sequentially ordered after each other

 */

namespace RDKit {

    PDBFileParser::PDBFileParser(string filename){
        setup(filename);
    }

    PDBFileParser::~PDBFileParser(){
    	delete protein;
    }

    string PDBFileParser::convert3LetterTo1LetterResidue(string threeCharResidue){
    	if (threeCharResidue == "ARG") {
    		return "R";
    	} else 	if (threeCharResidue == "HIS") {
    		return "H";
    	} else 	if (threeCharResidue == "LYS") {
    		return "K";
    	} else 	if (threeCharResidue == "ASP") {
    		return "D";
    	} else 	if (threeCharResidue == "GLU") {
    		return "E";
    	} else 	if (threeCharResidue == "SER") {
    		return "S";
    	} else 	if (threeCharResidue == "THR") {
    		return "T";
    	} else 	if (threeCharResidue == "ASN") {
    		return "N";
    	} else 	if (threeCharResidue == "GLN") {
    		return "Q";
    	} else 	if (threeCharResidue == "CYS") {
    		return "C";
    	} else 	if (threeCharResidue == "SEC") {
    		return "U";
    	} else 	if (threeCharResidue == "GLY") {
    		return "G";
    	} else 	if (threeCharResidue == "PRO") {
    		return "P";
    	} else 	if (threeCharResidue == "ALA") {
			return "A";
    	} else 	if (threeCharResidue == "VAL") {
			return "V";
    	} else 	if (threeCharResidue == "ILE") {
			return "I";
    	} else 	if (threeCharResidue == "LEU") {
			return "L";
    	} else 	if (threeCharResidue == "MET") {
			return "M";
    	} else 	if (threeCharResidue == "PHE") {
			return "F";
    	} else 	if (threeCharResidue == "TYR") {
			return "Y";
    	} else 	if (threeCharResidue == "TRP") {
			return "W";
    	} else {
    		ostringstream errout;
    		errout << "Unsupported residue: " << threeCharResidue;
    		throw std::runtime_error(errout.str());
    	}

    }

    double PDBFileParser::distance(float x1, float y1, float z1, float x2, float y2, float z2) {
    	double xd = x2-x1;
    	double yd = y2-y1;
    	double zd = z2-z1;
    	return sqrt((xd*xd) + (yd*yd) + (zd*zd));
    }

    void PDBFileParser::setupBonds(RWMol * mol,
									   vector<AtomRecord*> atomList) {
    	for(unsigned int i=0; i < atomList.size(); i++) {
    		for(unsigned int j=i+1; j < atomList.size(); j++) {
    			double dist = distance(atomList[i]->x, atomList[i]->y, atomList[i]->z,
										atomList[j]->x, atomList[j]->y, atomList[j]->z);

    			if (dist < BOND_THRESHOLD) {
    				mol->addBond(atomList[i]->atomId - 1, atomList[j]->atomId - 1, Bond::SINGLE); // FIXME
    				//cout << atomList[i]->atomId << " bound to " << atomList[j]->atomId << ": "<< dist << endl;
    			}
    		}
    	}
    }

    void PDBFileParser::setup(string filename) {

    	sequence = "";
    	protein = new RWMol();
    	atomRecords = vector<AtomRecord>();
    	map<string, RWMol *> ligands;


        ifstream pdbFile;
        string line; 
        string resId = "";
        ostringstream errout;

        map<string, vector<AtomRecord*> > residuesMap;

        pdbFile.open(filename.c_str(), ios::out);
        if (pdbFile.is_open()) { 

            while (getline(pdbFile, line)) {
				if (boost::starts_with(line, "ATOM") ) {
					AtomRecord * atmRec = new AtomRecord();
					getAtomRecord(line, atmRec);
					atomRecords.push_back(*atmRec);

					Atom atom(atmRec->elementSymbol);
					atom.setIdx(atmRec->atomId - 1);
					protein->addAtom(&atom); // add the atom to the mol
					//cout << atom.getIdx() << endl;


					if (resId != atmRec->residueId) {
						cout << atmRec->residueName << endl;
						if(resId != "") {
							setupBonds(protein, residuesMap[resId]);
						}

						sequence.append(convert3LetterTo1LetterResidue(atmRec->residueName));
						vector<AtomRecord*> atomRecList;
						atomRecList.push_back(atmRec);
						residuesMap[atmRec->residueId] = atomRecList;

					} else {

						residuesMap[atmRec->residueId].push_back(atmRec);

					}

					resId = atmRec->residueId;
					cout << line << "  :  "<< atmRec->residueId << atmRec->chainId << atmRec->residueName << atmRec->atomName << atmRec->x << atmRec->y << atmRec->z << atmRec->elementSymbol << endl;

					// setup bonds

				} else if (boost::starts_with(line, "HETATM")) {

					AtomRecord * atmRec = new AtomRecord();
					getAtomRecord(line, atmRec);
					atomRecords.push_back(*atmRec);

					Atom atom(atmRec->elementSymbol);
					atom.setIdx(atmRec->atomId - 1);
					protein->addAtom(&atom); // add the atom to the mol
					//cout << atom.getIdx() << endl;


					if (resId != atmRec->residueId) {
						cout << atmRec->residueName << endl;
						if(resId != "") {
							setupBonds(protein, residuesMap[resId]);
						}

						sequence.append(convert3LetterTo1LetterResidue(atmRec->residueName));
						vector<AtomRecord*> atomRecList;
						atomRecList.push_back(atmRec);
						residuesMap[atmRec->residueId] = atomRecList;

					} else {

						residuesMap[atmRec->residueId].push_back(atmRec);

					}

					resId = atmRec->residueId;
					cout << line << "  :  "<< atmRec->residueId << atmRec->chainId << atmRec->residueName << atmRec->atomName << atmRec->x << atmRec->y << atmRec->z << atmRec->elementSymbol << endl;

				}
	    	}
	    	pdbFile.close();
        } else {
            errout << "Bad input file " << filename;
            throw BadFileException(errout.str());
        }        

        //cout << MolToSmiles(*(static_cast<ROMol *>(protein)),true) << endl;
        cout << sequence << endl;
    }

    string PDBFileParser::getInsterestingSubstr(string line, int pos, int len) {
    	string interestingSubstr;
		interestingSubstr = line.substr(pos, len);
		boost::trim(interestingSubstr);
		return interestingSubstr;
    }
    
    void PDBFileParser::getAtomRecord(string pdbLine, AtomRecord * atomRecord) {

		// 0 based positions unlike the spec so -1
    	atomRecord->atomId = atoi(getInsterestingSubstr(pdbLine, 7, 5).c_str());
		atomRecord->residueId = getInsterestingSubstr(pdbLine, 22, 5);
		atomRecord->chainId = getInsterestingSubstr(pdbLine, 21, 1);
		atomRecord->residueName = getInsterestingSubstr(pdbLine, 17, 3);
		atomRecord->atomName = getInsterestingSubstr(pdbLine, 12, 4);
		atomRecord->x = atof(getInsterestingSubstr(pdbLine, 30, 8).c_str());
		atomRecord->y = atof(getInsterestingSubstr(pdbLine, 38, 8).c_str());
		atomRecord->z = atof(getInsterestingSubstr(pdbLine, 46, 8).c_str());
		atomRecord->elementSymbol = getInsterestingSubstr(pdbLine, 76, 2);

    }

  
}
 





