// $Id$
//
//  Copyright (C) 2012 JP
//
//  @@ All Rights Reserved @@
//  This file is part of the RDKit.
//  The contents are covered by the terms of the BSD license
//  which is included in the file license.txt, found at the root
//  of the RDKit source tree.
//
#ifndef _PDB_FILE_PARSER
#define _PDB_FILE_PARSER

#include <string>
#include <map>

#include <GraphMol/RDKitBase.h>

using namespace std;

namespace RDKit {

class PDBFileParser {

public:
	string sequence;
	PDBFileParser(string filename);
	~PDBFileParser();
	static string convert3LetterTo1LetterResidue(string threeCharResidue);
	void setup(string filename);
	struct AtomRecord {
		int atomId; // serial
		string chainId;
		string residueId; // concat resSeq and iCode
		string residueName;
		string atomName;
		float x;
		float y;
		float z;
		string elementSymbol;
	};


private:
	RWMol *protein; // protein moleule
	map<string, RWMol *> ligands;

	vector<AtomRecord> atomRecords;
	void getAtomRecord(string pdbLine, AtomRecord * atomRecord);
	string getInsterestingSubstr(string line, int pos, int len);
	void setupBonds(RWMol * mol, vector<AtomRecord*> atomList);
	double distance(float x1, float y1, float z1, float x2, float y2, float z2);

};

}

#endif
